library(tidyverse)

#part1
part1 <- read.table("input/input1") %>% 
  crossing(x = ., y= .) %>% 
  filter(x + y == 2020) %>% 
  unlist() %>% 
  unique() %>% 
  prod()

#part2
part2 <- read.table("input/input1") %>% 
  crossing(x = ., y= ., z = .) %>% 
  filter(x + y + z == 2020) %>% 
  unlist %>% 
  unique() %>% 
  prod()
